<h1 align="center">CLI Table</h1>

To see a table based on an array of data, you need to open the folder with files through the terminal and execute one of the commands:

```bash
php cli
```
Or

```bash
php index.php
```
The file `recivedData.php` contains the main array with data(variable $arr) that will be used to display in the table.
It can be supplemented or replaced with another array with the same nesting and record key value.
Quantity key => value in subarrays can be different and of any length.
The variable is `$columnWidth` responsible for the width of the columns in the table. 
The variable `$checkArr` stores the response value (whether the array structure is appropriate for the code to work correctly)

The function `echoLineSeparator($sign)` is used in a file `index.php` and `cli` to output a separator line. The character specified instead of the `$sign` variable will serve as a delimiter.
Files `cli` and `index.php` are responsible for displaying the table.

## IMPORTANT
- **So that the structure of the `$arr` and the nesting remain the same. Otherwise, the code will break**
- **What would the value of `$columnWidth` be greater than the maximum length of the key and value in the array**