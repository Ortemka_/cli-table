<?php

// Data for the table
$arr = [
    [
        'House' => 'Baratheon',
        'Sigil' => 'A crowned stag',
        'Motto' => 'Ours is the Fury',
    ],
    [
        'Leader' => 'Eddard Stark',
        'House' => 'Stark',
        'Motto' => 'Winter is Coming',
        'Sigil' => 'A gray direwolf',
    ],
    [
        'House' => 'Lannister',
        'Leader' => 'Tywin Lannister',
        'Sigil' => 'A golden lion',
    ],
    [
        'Q' => 'Z', 
    ],
];




// Column width
$columnWidth = 25;

// An array of unique keys
$allUniqueKeys = [];

//Is the array structure correct?
$checkArr = true;

// Checking that the $arr data type is an array and it is not empty
if(!is_array($arr) || empty($arr)){
    $checkArr = false;
}

// Loop to get unique keys
foreach ($arr as $index => $subArr) {
    if(!is_array($subArr)){ // Checking if an array is nested
        $checkArr = false;
    }
   
    foreach($subArr as $textKey => $value) {
        if (!in_array($textKey, $allUniqueKeys)) {
            $allUniqueKeys[] = $textKey;
        }
    }
}

// Sort unique keys alphabetically
sort($allUniqueKeys);

// Outputs a delimiter with the specified sign to the length of the entire table
function echoLineSeparator($sign) {
    global $allUniqueKeys, $columnWidth;
    echo str_pad('', (count($allUniqueKeys)) * $columnWidth + 1, $sign);
    echo "\n";
}
?>