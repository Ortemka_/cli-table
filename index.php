<?php

// Connection of array and delimiter function
require_once 'recivedData.php';

// Checking array structure before output
if($checkArr === false) {
    
    echo "\n";
    echoLineSeparator('=');
    echo 'There is no data in the array or its structure is broken';
    echo "\n";
    echoLineSeparator('=');

}else{

    // Header delimeter
    echo "\n";
    echoLineSeparator('=');
    echo '|';

    // Unique keys in an array
    foreach($allUniqueKeys as $key) {
        echo str_pad( $key . "|", $columnWidth, ' ', STR_PAD_LEFT);
    }
    echo "\n";

    // Delimeter
    echoLineSeparator('-');

    // The main body of the table
    for($k = 0; $k < count($arr); $k++ ){
        echo '|';
        foreach($allUniqueKeys as $key){
            if(array_key_exists($key, $arr[$k])) {
                echo str_pad( $arr[$k][$key] . '|', $columnWidth, ' ', STR_PAD_LEFT);
            }else{
                echo str_pad(' |', $columnWidth, ' ' , STR_PAD_LEFT);
            }
        }
        echo "\n";
    }

    // Footer delimeter
    echoLineSeparator('=');
}
?>